import { DataSource } from 'typeorm'
import { ExtendedUsers } from '../domains/extended_users/models/extended_users.schema'
import { Permissions } from '../domains/permission/models/permissions.schema'
import { Users } from '../domains/users/models/users.schema'

const MySqlDataSource = new DataSource({
  type: 'mysql',
  host: 'localhost',
  port: 3306,
  username: 'root',
  password: 'root',
  database: 'snt_solutions',
  synchronize: true,
  logging: true,
  entities: [Users, ExtendedUsers, Permissions ],
  subscribers: [],
  migrations: [],
})
const connectDB = () => {
  MySqlDataSource.initialize()
    .then(() => {
      console.log('Successful database connection!')
    })
    .catch((err) => {
      console.log('Database connection failed!', err)
    })
}
export { MySqlDataSource, connectDB }
