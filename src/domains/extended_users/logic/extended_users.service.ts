import { MySqlDataSource } from '../../../config/connectDB'
import { IFCreateOneExtendUser, IFFindExtendUser } from '../models/extended_users.interface'
import { ExtendedUsers } from '../models/extended_users.schema'

const extendedUsersRepository = MySqlDataSource.getRepository(ExtendedUsers)

export const createOneExtendUser = (data: IFCreateOneExtendUser) => {
    return extendedUsersRepository.save(data)
}

export const findOneExtendUser = (query: IFFindExtendUser[], selectPassword?: boolean): Promise<ExtendedUsers | null> => {
    return extendedUsersRepository.findOne({
        where: [...query], select: { password: selectPassword }
    })
}
