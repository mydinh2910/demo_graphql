export interface IFCreateOneExtendUser {
    userID: string
    email: string
    password: string
    phoneNumber?: string
    googleID?: string
    postalCode?: number
    addressLine1?: string
    addressLine2?: string
}

export interface IFFindExtendUser {
    userID?: string
    email?: string
    phoneNumber?: string
}
