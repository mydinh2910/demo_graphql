import { Column, CreateDateColumn, DeleteDateColumn, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm'
import { Users } from '../../users/models/users.schema'

@Entity()
export class ExtendedUsers {
    @PrimaryGeneratedColumn('uuid')
    extendedUserID: string

    @OneToOne(() => Users, user => user.userID, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'userID' })
    @Column({ type: 'varchar', nullable: false })
    userID: string

    @Column({ type: 'varchar', length: 36, unique: true, nullable: true })
    email: string

    @Column({ type: 'varchar', length: 15, unique: true, nullable: true })
    phoneNumber: string

    @Column({ type: 'varchar', unique: true, nullable: true })
    googleID: string

    @Column({ type: 'varchar', default: null, select: false })
    password: string

    @Column({ type: 'int', nullable: true })
    postalCode: number

    @Column({ type: 'text', default: null })
    addressLine1: string

    @Column({ type: 'text', default: null })
    addressLine2: string

    @CreateDateColumn()
    createdAt: Date

    @UpdateDateColumn()
    updatedAt: Date

    @DeleteDateColumn()
    deletedAt: Date
}