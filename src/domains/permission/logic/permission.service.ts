import { MySqlDataSource } from '../../../config/connectDB'
import { IFCreateOnePermissions } from '../models/permissions.interface'
import { Permissions } from '../models/permissions.schema'

const permissionRepository = MySqlDataSource.getRepository(Permissions)

export const createOnePermission = (data: IFCreateOnePermissions): Promise<Permissions> => {
    return permissionRepository.save(data)
}