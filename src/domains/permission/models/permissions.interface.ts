import { PermissionsRoleName, PermissionsStatus } from './permissions.schema'

export interface IFCreateOnePermissions {
    userID: string,
    status: PermissionsStatus,
    roleName: PermissionsRoleName
}