import {
    Column,
    CreateDateColumn,
    DeleteDateColumn,
    Entity,
    JoinColumn,
    ManyToOne,
    PrimaryGeneratedColumn,
    UpdateDateColumn
} from 'typeorm'

import { Users } from '../../users/models/users.schema'

export enum PermissionsStatus {
    ACCEPTED = 'ACCEPTED',
    PENDING = 'PENDING'
}

export enum PermissionsRoleName {
    ADMIN = 'ADMIN',
    USER = 'USER',
}

@Entity()
export class Permissions {
    @PrimaryGeneratedColumn('uuid')
    permissionID: string

    @ManyToOne(() => Users, user => user.userID, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'userID' })
    @Column({ type: 'varchar', nullable: false })
    userID: string

    @Column({ type: 'enum', enum: PermissionsStatus, nullable: false })
    status: PermissionsStatus

    @Column({ type: 'enum', enum: PermissionsRoleName, nullable: false })
    roleName: PermissionsRoleName

    @CreateDateColumn()
    createdAt: Date

    @UpdateDateColumn()
    updatedAt: Date

    @DeleteDateColumn()
    deletedAt: Date
}