import bcrypt from 'bcrypt'
import { createOneExtendUser, findOneExtendUser } from '../../extended_users/logic/extended_users.service'
import { createOnePermission } from '../../permission/logic/permission.service'
import { createOneUser } from './users.service'
import { IFCreateOneUserWithAccountName } from '../models/users.interface'
import { PermissionsRoleName, PermissionsStatus } from '../../permission/models/permissions.schema'
import ApiError from '../../../helper/ApiError'

export const createOneUserWithAccountName = async (data: IFCreateOneUserWithAccountName) => {
    const {
        displayName,
        firstName,
        lastName,
        gender,
        dateOfBirth,
        avatar,
        background,
        country,
        summary,
        emotion,
        email,
        phoneNumber,
        password,
        postalCode,
        addressLine1,
        addressLine2,

    }: IFCreateOneUserWithAccountName = data

    const existedExtendUser = await findOneExtendUser([{email},{phoneNumber}])

    if (existedExtendUser) {
        const msg: string[] = []

        if (existedExtendUser.email === email) {
            msg.push(`email`)
        }

        if (existedExtendUser.phoneNumber === phoneNumber) {
            msg.push(`phoneNumber`)
        }

        throw new ApiError(401, `${msg.join(',')} already exists`)
    }

    const user = await createOneUser({
        emotion, avatar, background, country, dateOfBirth,
        displayName, firstName, gender, lastName, summary
    })

    await createOnePermission({ roleName: PermissionsRoleName.USER, status: PermissionsStatus.ACCEPTED, userID: user.userID })

    const hashPassword = bcrypt.hashSync(password, bcrypt.genSaltSync(10))

    await createOneExtendUser({
        password: hashPassword, userID: user.userID, addressLine1
        , addressLine2, email, phoneNumber, postalCode
    })
}