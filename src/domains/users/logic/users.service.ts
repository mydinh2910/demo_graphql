import bcrypt from 'bcrypt'
import { MySqlDataSource } from '../../../config/connectDB'
import ApiError from '../../../helper/ApiError'
import { signToken } from '../../../helper/handleToken'
import { ExtendedUsers } from '../../extended_users/models/extended_users.schema'
import { IFCreateOneUser, IFSignInUser } from '../models/users.interface'
import { Users, UserStatus } from '../models/users.schema'

const usersRepository = MySqlDataSource.getRepository(Users)

export const createOneUser = (data: IFCreateOneUser): Promise<Users> => {
    return usersRepository.save(data)
}

export const findOneUser = async (userID: string): Promise<Users | null> => {
    return usersRepository.findOneBy({ userID })
}

export const signInUser = async (requiredDataSignIn: IFSignInUser): Promise<string> => {
    const existedUser = await MySqlDataSource.getRepository(ExtendedUsers)
        .createQueryBuilder('eu')
        .leftJoinAndSelect('eu.userID', 'u')
        .select('u.userID', 'userID')
        .addSelect('u.status', 'status')
        .addSelect('eu.password', 'password')
        .where('eu.email = :email', { email: requiredDataSignIn.email })
        .getRawOne()

    if (!existedUser) {
        throw new ApiError(401, 'Account does not exist')
    }

    if (existedUser?.status !== UserStatus.ACTIVE) {
        throw new ApiError(401, 'User is not activated')
    }

    if (!bcrypt.compareSync(requiredDataSignIn.password, existedUser?.password)) {
        throw new ApiError(401, 'Incorrect password')
    }

    const token = signToken({ userID: existedUser.userID })

    return token
}