import { Permissions } from '../../permission/models/permissions.schema'

export interface IFCreateOneUserWithAccountName {
    email: string,
    phoneNumber?: string,
    password: string,
    displayName?: string,
    firstName?: string,
    lastName?: string,
    gender?: string,
    dateOfBirth?: Date,
    avatar?: string,
    background?: string,
    country?: string,
    summary?: string,
    emotion?: string,
    postalCode?: number,
    addressLine1?: string,
    addressLine2?: string,
}
export interface IFCreateOneUser {
    displayName?: string,
    firstName?: string,
    lastName?: string,
    gender?: string,
    dateOfBirth?: Date,
    avatar?: string,
    background?: string,
    country?: string,
    summary?: string,
    emotion?: string
}
export interface IFSignInUser {
    email: string,
    password: string
}
export interface IFCredentials {
    userID: string,
    status: string,
    displayName: string,
    firstName: string,
    lastName: string,
    gender: string,
    dateOfBirth: Date,
    avatar: string,
    background: string,
    country: string,
    summary: string,
    emotion: string,
    permissions: Permissions[],
    createdAt: Date,
    updatedAt: Date,
    deletedAt: Date
    isAdmin?: boolean
}
