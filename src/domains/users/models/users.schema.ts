import {
  Entity,
  Column,
  OneToOne,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  OneToMany,
} from 'typeorm'
import { ExtendedUsers } from '../../extended_users/models/extended_users.schema'
import { Permissions } from '../../permission/models/permissions.schema'

export enum UserStatus {
  ACTIVE = 'ACTIVE',
  INACTIVE = 'INACTIVE',
  DEACTIVATED = 'DEACTIVATED',
}

@Entity()
export class Users {
  @PrimaryGeneratedColumn('uuid')
  userID: string

  @Column({ type: 'enum', enum: UserStatus, nullable: false, default: UserStatus.ACTIVE })
  status: string

  @Column({ type: 'varchar', default: null })
  displayName: string

  @Column({ type: 'varchar', length: 50, default: null })
  firstName: string

  @Column({ type: 'varchar', length: 50, default: null })
  lastName: string

  @Column({ type: 'varchar', length: 10, default: null })
  gender: string

  @Column({ type: 'datetime', default: null })
  dateOfBirth: Date

  @Column({ type: 'varchar', default: null })
  avatar: string

  @Column({ type: 'varchar', nullable: true })
  background: string

  @Column({ type: 'varchar', nullable: true })
  country: string

  @Column({ type: 'varchar', default: null })
  summary: string

  @Column({ type: 'varchar', default: null })
  emotion: string

  @OneToOne(() => ExtendedUsers, extendUser => extendUser.userID, { onDelete: 'SET NULL' })
  extendedUser: ExtendedUsers

  @OneToMany(() => Permissions, permissions => permissions.userID)
  permissions: Permissions[]

  @CreateDateColumn()
  createdAt: Date

  @UpdateDateColumn()
  updatedAt: Date

  @DeleteDateColumn()
  deletedAt: Date
}
