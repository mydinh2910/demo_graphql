import { createOneUserWithAccountName } from "./logic/users.multiple_service"
import { signInUser } from "./logic/users.service"

export const Mutation = {

    signUp: async (parent: any, args: any, context: any, info: any) => {
        await createOneUserWithAccountName(args.data)

        return true
    },

    signIn: async (parent: any, args: any, context: any, info: any) => {
        const token = await signInUser(args.data)

        return token
    },
}