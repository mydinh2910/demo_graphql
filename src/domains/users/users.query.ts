import { convertDateToString } from "../../helper/ConvertDateToString"
import { findOneExtendUser } from "../extended_users/logic/extended_users.service"
import { findOneUser } from "./logic/users.service"

export const Query = {
    user: async (parent: any, args: any, context: any, info: any) => {
        const user = await findOneUser(args.id)
        
        return convertDateToString(user)
    }
}

export const User = {
    extended_users: async (parent: any, args: any, context: any, info: any)=> {
        const existedUser = await findOneExtendUser([{ userID: parent?.userID }])

        return convertDateToString(existedUser)
    }
}