import { Query, User } from "./users.query"
import { Mutation } from './users.mutation'

const usersResolvers = {
    Mutation,
    Query,
    User
}

export default usersResolvers