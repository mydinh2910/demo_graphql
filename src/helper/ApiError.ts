class ApiError {
  public statusCode: number
  public message: string
  constructor(statusCode: number, message: string) {
    this.message = message
    this.statusCode = statusCode
  }
}
export default ApiError
