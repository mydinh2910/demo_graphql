export const convertDateToString = (data: any) => {

    if (typeof data !== 'object') {
        return
    }

    const result: any = {}

    for (const [key, value] of Object.entries(data)) {

        if (value instanceof Date) {
            result[key] = data[key].toDateString()
            continue
        }

        result[key] = data[key]
    }

    return result
}