import Jwt from 'jsonwebtoken'

export const signToken = (payload: object) => {
    const TOKEN_SECRET = process.env.TOKEN_SECRET as string

    const expiresIn = (Number(process.env.TOKEN_EXPIRED) * 60)

    return Jwt.sign(payload, TOKEN_SECRET, { expiresIn })
}

export const verifyToken = (token: string) => {
    const TOKEN_SECRET = process.env.TOKEN_SECRET as string

    return Jwt.verify(token, TOKEN_SECRET)
}