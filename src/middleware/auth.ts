import { verifyToken } from '../helper/handleToken'


export const authJwt = async ({ req, res }: { req: any, res: any }) => {

    const token = req.headers.authorization || ''

    if (token && token.split(' ')[0] === 'Bearer') {
        const decodeToken = verifyToken(token.split(' ')[1])

        return {
            user: {
                userID: (decodeToken as any)?.userID
            }
        }
    }

    return null
}