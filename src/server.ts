import express from 'express'
import { ApolloServer } from 'apollo-server-express'
import { GraphQLFileLoader } from '@graphql-tools/graphql-file-loader';
import { makeExecutableSchema } from '@graphql-tools/schema'
import { loadSchema } from '@graphql-tools/load';
import { connectDB } from './config/connectDB'
import { rootResolvers } from './root.resolvers';
import path from 'path';
import dotenv from 'dotenv'
import { authJwt } from './middleware/auth';

dotenv.config()
const app = express()

const init = async () => {

    const rootTypeDefs = await loadSchema(path.join(__dirname, './root.typedefs.graphql'), {
        loaders: [
            new GraphQLFileLoader()
        ]
    });

    const schema = makeExecutableSchema({
        typeDefs: rootTypeDefs,
        resolvers: rootResolvers
    })

    const server = new ApolloServer({
        schema,
        context: authJwt
    });

    server.start().then(() => {
        server.applyMiddleware({ app })
        connectDB()
    })

    const port = 3000

    app.listen(port, () => {
        console.log(`⚡️[Server]: Server is running at http://localhost:${port}${server.graphqlPath}`)
    })

}

init().catch(err => console.log(err))




